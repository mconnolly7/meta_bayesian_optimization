import numpy as np
import matplotlib.pyplot as plt

load_path = '../data/cep_rand_rewards_best.npy'
load_path = '../data/dlep_rand_rewards_best.npy'
rand_data = np.load(load_path)

load_path = '../data/cep_bao_rewards_best.npy'
load_path = '../data/dlep_bao_rewards_best.npy'
bao_data = np.load(load_path)

load_path = '../data/cep_naf_rewards_best.npy'
load_path = '../data/dlep_naf_rewards_best.npy'
naf_data = np.load(load_path)

#--- Rand
rand_mean = np.mean(rand_data, axis=0)
rand_std = np.std(rand_data, axis=0)
rand_ci = rand_std / np.sqrt(100) * 1.96

rand_m50 = np.percentile(rand_data, 50, axis=0)
rand_m25 = np.percentile(rand_data, 25, axis=0)
rand_m75 = np.percentile(rand_data, 75, axis=0)

#--- BAO
bao_mean = np.mean(bao_data, axis=0)
bao_std = np.std(bao_data, axis=0)
bao_ci 	= bao_std / np.sqrt(100) * 1.96

bao_m50 = np.percentile(bao_data, 50, axis=0)
bao_m25 = np.percentile(bao_data, 25, axis=0)
bao_m75 = np.percentile(bao_data, 75, axis=0)

#--- NAF
naf_mean = np.mean(naf_data, axis=0)
naf_std = np.std(naf_data, axis=0)
naf_ci 	= naf_std / np.sqrt(100) * 1.96

naf_m50 = np.percentile(naf_data, 50, axis=0)
naf_m25 = np.percentile(naf_data, 25, axis=0)
naf_m75 = np.percentile(naf_data, 75, axis=0)


# line1 = ax.plot(bao_m50,'k-', linewidth=2, label='Upper Confidence Bound')
# line1 = ax.plot(bao_m25,'k-', linewidth=2, label='Upper Confidence Bound')
# line1 = ax.plot(bao_m75,'k-', linewidth=2, label='Upper Confidence Bound')

# line2 = ax.plot(naf_m50,'k--', linewidth=2,  label='Neural Acquisition Function')
# line2 = ax.plot(naf_m25,'k--', linewidth=2,  label='Neural Acquisition Function')
# line2 = ax.plot(naf_m75,'k--', linewidth=2,  label='Neural Acquisition Function')
plt.rcParams.update({'font.size': 20})

# Curves
fig, (ax0) = plt.subplots(1,1, figsize=(8, 5))


color = [0.4588, 0.4392, 0.7020]
ax0.fill_between(range(10), naf_mean - naf_ci, naf_mean + naf_ci,
                 color=color, alpha=0.5)
line2 = ax0.plot(naf_mean,'-', linewidth=2,  label='GP-NAF', color=color)

color = [0.8510, 0.3725, 0.0078]
ax0.fill_between(range(10), bao_mean - bao_ci, bao_mean + bao_ci,
                 color=color, alpha=0.5)
line2 = ax0.plot(bao_mean,'-', linewidth=2,  label='GP-UCB', color=color)

color = [0.1059, 0.6196, 0.4667]
ax0.fill_between(range(10), rand_mean - rand_ci, rand_mean + rand_ci,
                 color=color, alpha=0.5)
line2 = ax0.plot(rand_mean,'-', linewidth=2,  label='RAND', color=color)

ax0.set_xlim([0,9])
ax0.set_xlabel('Samples')
ax0.set_ylabel('Reward (a.u.)')
ax0.legend()
ax0.spines['top'].set_visible(False)
ax0.spines['right'].set_visible(False)
fig.tight_layout()

plt.draw()
plt.savefig('../figures/trajectory_error.png')
plt.close()

rand_cumm_err = np.mean(rand_data[:,0:9], axis=1) * 10
bao_cumm_err = np.mean(bao_data[:,0:9], axis=1) * 10
naf_cumm_err = np.mean(naf_data[:,0:9], axis=1) * 10


# Box plots
fig, (ax1) = plt.subplots(1,1, figsize=(7, 5))

box = ax1.boxplot([rand_cumm_err, bao_cumm_err, naf_cumm_err], notch=True, patch_artist=True, vert=False, whiskerprops=dict(linewidth=2), boxprops=dict(linewidth=2))

colors = [[0.1059, 0.6196, 0.4667], [0.8510, 0.3725, 0.0078], [0.4588, 0.4392, 0.7020]]
for patch, color in zip(box['boxes'], colors):
    patch.set_edgecolor(color)
    patch.set_facecolor([1,1,1])
    
ax1.set_xlabel('Cumulative Reward (a.u.)')
ax1.set_yticks([1,2,3])
ax1.set_yticklabels( ['Random', 'GP-UCB', 'GP-NAF'])

ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(False)
fig.tight_layout()

plt.draw()
plt.savefig('../figures/cummulative_error.png')
plt.close()














