import sys
import math

import numpy as np
import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.utils as utils
from torch.distributions import Categorical
import torchvision.transforms as T

import matplotlib.pyplot as plt

import time
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern, RBF, WhiteKernel, ConstantKernel as C



class BAYES_OPT:
    def __init__(self, acquisition_function, parameter):
        self.acquisition_function = acquisition_function
        self.parameter = parameter

    def select_action(self, x_data, y_data, input_space, n_burn_in, max_samples, i_episode, i_trial):

        if not x_data: 
            n_samples = 0
        else:
            n_samples = np.shape(x_data)[0]

        # n_samples_feature = np.ones((input_space.shape[0], 1))*n_samples
        # max_samples_feature = np.ones((input_space.shape[0], 1))*max_samples

        # Only fit the GP if enough samples and the length scales are non-zero
        gp_mu = .01*np.random.randn(input_space.shape[0], 1)
        gp_sigma_sq = .01*np.random.randn(input_space.shape[0], 1) 
        
        acquisition_score = gp_mu
        if n_samples > n_burn_in:

            length_scale = np.std(input_space, axis=0) / np.sqrt(2)
            noise = np.std(y_data, axis=0) / np.sqrt(2)
            
            if ~np.any(length_scale == 0):
                
                kernel =  Matern(length_scale=length_scale, nu=2.5, length_scale_bounds='fixed') + WhiteKernel(noise_level=0.1,noise_level_bounds='fixed')
                # kernel =  Matern(length_scale=length_scale, nu=2.5, length_scale_bounds='fixed') + WhiteKernel(noise_level=.01)
                gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=10, normalize_y=True)

                # Fit to data using Maximum Likelihood Estimation of the parameters
                gp.fit(x_data, y_data)
                
                # Evaluate input on GP
                gp_mu, gp_sigma_sq = gp.predict(input_space, return_std=True)                 

                gp_mu = np.expand_dims(gp_mu, axis=1)
                gp_sigma_sq = np.expand_dims(gp_sigma_sq, axis=1)

                # Select the next input based on the acquisition function
                acquisition_score = gp_mu + self.parameter * np.log(n_samples) * gp_sigma_sq

        action_idx = np.argmax(acquisition_score)
        next_input = input_space[action_idx,:]

        #----------
        # Plotting 
        #----------
        checkpoints = [0, 2,  4,  9, 19]
        # checkpoints = []

        if n_samples in checkpoints and i_episode % 10 == 0 and i_trial == 0: 
            np_y_data = np.array(y_data)
            np_x_data = np.array(x_data)

            nd = 100;
            x1 = np.reshape(input_space[:,0], (nd,nd))
            x2 = np.reshape(input_space[:,1], (nd,nd))

            fig = plt.figure(figsize=plt.figaspect(0.5))

            # a0 = np.reshape(policy_logits.data.numpy(), (nd,nd))
            # cm = plt.cm.get_cmap('viridis')
            # ax0 = fig.add_subplot(2, 2, 3, projection='3d')
            # surf = ax0.plot_surface(x1, x2, a0, cmap=cm, linewidth=0, antialiased=False)

            a1 = np.reshape(gp_mu, (nd,nd))
            cm1 = plt.cm.get_cmap('viridis')
            ax1 = fig.add_subplot(2, 2, 1, projection='3d')
            surf1 = ax1.plot_surface(x1, x2, a1, cmap=cm1, linewidth=0, antialiased=False)
            ax1.set_zlim([0, 1])

            # a2 = np.reshape(estimated_values.data.numpy(), (nd,nd))
            # cm2= plt.cm.get_cmap('viridis')
            # ax2 = fig.add_subplot(2, 2, 4, projection='3d')
            # surf = ax2.plot_surface(x1, x2, a2, cmap=cm2, linewidth=0, antialiased=False)

            a3 = np.reshape(gp_sigma_sq, (nd,nd))
            cm3= plt.cm.get_cmap('viridis')
            ax3 = fig.add_subplot(2, 2, 2, projection='3d')
            surf = ax3.plot_surface(x1, x2, a3, cmap=cm3, linewidth=0, antialiased=False)

            figure_name = "../figures/training_telemetry_{}.png".format(n_samples)
            print(figure_name)
            plt.savefig(figure_name)
        
            plt.close()
            
        log_prob = 0
        estimated_value = 0
            
        return next_input, log_prob, estimated_value

    def update_parameters(self, loss):      
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

    def discounted_rewards(self, rewards, gamma):
        R = 0
        discounted_rewards = []

        for i in reversed(range(len(rewards))):
            # Recursive: G(t) = r(t) + G(t+1)^DISCOUNT
            R = gamma * R + rewards[i]

            # Add to the FRONT of rewards
            discounted_rewards.insert(0, R)

        discounted_rewards = torch.tensor(discounted_rewards)
        
        #--------
        # Whitening the returns does not work well on this problem. Probably, because
        # it changes dramatically when the algorithm repeatedly samples from high quality
        # areas of the parameter space - the whole objective. May be useful in other contexts.
        #--------

        # discounted_rewards = (discounted_rewards - discounted_rewards.mean()) / discounted_rewards.std()

        return discounted_rewards








