import argparse, math, os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import torch
import torch.nn.utils as utils
import torch.nn as nn
from torch.autograd import Variable

from neural_acquisition_function import REINFORCE
from bayesian_optimization import BAYES_OPT
from objective import quadratic_objective, easom_objective, gp_objective_function
from load_gp_objective import load_gp_objective

from tempfile import TemporaryFile

seed = 1234
torch.manual_seed(seed)
np.random.seed(seed)

PATH = '../saved_models/cep_training_2022_01_05/reinforce-800.pkl'
PATH = '../saved_models/cep_training_2022_01_07/reinforce-220.pkl'
checkpoint = torch.load(PATH)

# Training protocol
gamma = .95
learning_rate = .001

num_steps = 10
n_burn_in = 1
num_episodes = 30

# Model architecture
n_objective_inputs = 2
n_gp_outputs = 2
n_sample_features = 2

n_af_inputs = n_objective_inputs + n_gp_outputs + n_sample_features
n_af_outputs = 1
hidden_size = 128

acquisition_function = 'bao'
objective_function = 'DLEP'

#--------------------
# Acquisition Function 
#--------------------
if acquisition_function == 'naf':
    optimizer = REINFORCE(hidden_size, n_af_inputs, n_af_outputs, learning_rate)
    optimizer.model.load_state_dict(checkpoint)
    n_burn_in = 1
elif acquisition_function =='bao':
    optimizer = BAYES_OPT('ucb', 0.4)
    n_burn_in = 4

elif acquisition_function =='rand':
    optimizer = BAYES_OPT('ucb', 0.4)
    n_burn_in = num_steps

#--------------------
# Objective Function 
#--------------------
if objective_function == 'CEP':
    # Set up the objective function
    df = pd.read_csv("../data/cep_data.csv")
    objective_data = df.to_numpy()
    x_data = objective_data[:,(0,1)]
    y_data = objective_data[:,2]

    # Set up input space
    x1 = np.linspace(1, 15, 100)
    x2 = np.linspace(0, 300, 100)

    gp_objective_model = load_gp_objective(x_data, y_data)

elif objective_function == 'DLEP':
    # Set up the objective function
    df = pd.read_csv("../data/dlep_data_2.csv")
    objective_data = df.to_numpy()
    x_data = objective_data[:,(0,1)]
    y_data = objective_data[:,2]

    # Set up input space
    x1 = np.linspace(40, 50, 100)
    x2 = np.linspace(0, 300, 100)

    gp_objective_model = load_gp_objective(x_data, y_data)
elif objective_function == 'easom':
    # Set up input space
    x1 = np.linspace(-5, 5, 100)
    x2 = np.linspace(-5, 5, 100)

input_space = np.array(np.meshgrid(x1, x2)).T.reshape(-1, n_objective_inputs)

all_reward_last = []
all_reward_best = []
    
# plt.ion()

for i_episode in range(num_episodes):

    trial_reward_last = []
    trial_reward_best = []
    
    x_data = []
    y_data = []
    x_max_data = []

    # Randomize offset within one quadrant of the parameter space
    # offset_1 = np.random.random() * 5 - 2.6  
    # offset_2 = np.random.random() * -150 + 30
    offset_1 = 0
    offset_2 = 0

    offset = [offset_1, offset_2]

    y_gt = gp_objective_model.predict(input_space - offset)

    y_gt_max = y_gt.max()
    y_gt_min = y_gt.min()

    # Run trial 
    for t in range(num_steps):

        next_input, log_prob, value_estimate = optimizer.select_action(x_data, y_data, input_space, n_burn_in, num_steps, i_episode, -1)

        # Get y data with noise based on next input
        if objective_function == 'CEP' or objective_function == 'DLEP':
            y = gp_objective_function(next_input, offset, gp_objective_model) + .01*np.random.randn() 
            y = (y - y_gt_min) / (y_gt_max - y_gt_min)
        elif objective_function == 'easom':
            y = easom_objective(next_input, offset) + .01*np.random.randn() 

        y_data.append(y)
        x_data.append(next_input)

        # Calculate the best input so far
        y_max = max(y_data)
        y_max_idx = y_data.index(y_max)
        best_input = x_data[y_max_idx]
        x_max_data.append(best_input)

        reward_last = y
        if objective_function == 'CEP' or objective_function == 'DLEP':
            reward_best = gp_objective_function(best_input, offset, gp_objective_model) + .01*np.random.randn()
            reward_best = (reward_best - y_gt_min) / (y_gt_max - y_gt_min)
        elif objective_function == 'easom':
            reward_best = easom_objective(best_input, offset) + .01*np.random.randn() 

        # Store probability, rewards, and value for this sample
        trial_reward_last.append(reward_last)
        trial_reward_best.append(reward_best)

    all_reward_last.append(trial_reward_last)
    all_reward_best.append(trial_reward_best)

    #----------
    # Plotting 
    #----------

    x_max_idx = np.argmax(y_gt)
    x_max = input_space[x_max_idx,:]

    np_x_data = np.array(x_data)
    np_x_max_data = np.array(x_max_data)
    
    np_y_data = np.array(y_data)
    np_rewards_best = np.array(all_reward_best)

    # plt.rcParams.update({'font.size': 16})

    # fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, ncols=1, sharex=False, figsize=(10, 6))
    # # fig, (ax0, ax1) = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(6, 6))

    # color = [0.4588, 0.4392, 0.7020]
    # ax0.plot([0, num_steps-1], [x_max[0], x_max[0]], linestyle='dashed', linewidth=3, color=[.5,.5,.5])
    # ax0.plot(np_x_data[:,0], linewidth=2, linestyle='dotted', color=[0,0,.0])
    # ax0.plot(np_x_max_data[:,0], linewidth=2, color=color)
    # # ax0.set_ylim([.9,15.1])
    # ax0.spines['top'].set_visible(False)
    # ax0.spines['right'].set_visible(False)
    # ax0.set_ylabel('Depth (mm)')
    # ax0.set_xticklabels([])

    # ax1.plot([0, num_steps-1], [x_max[1], x_max[1]], linestyle='dashed', linewidth=3, color=[.5,.5,.5])
    # ax1.plot(np_x_data[:,1], linewidth=2, linestyle='dotted', color=[0,0,.0])
    # ax1.plot(np_x_max_data[:,1], linewidth=2, color=color)
    # ax1.set_ylim([-10, 310])
    # ax1.spines['top'].set_visible(False)
    # ax1.spines['right'].set_visible(False)
    # ax1.set_ylabel('Amplitude (uA)')
    # ax1.set_xticklabels([])

    # ax2.plot([0, num_steps-1], [1,1], linestyle='dashed', linewidth=3, color=[.5,.5,.5])
    # ax2.plot(np_y_data[0], linewidth=2, linestyle='dotted', color=[0,0,.0])
    # ax2.plot(np_rewards_best[0], linewidth=2, color=color)
    # # ax2.set_ylim([-10, 310])
    # ax2.spines['top'].set_visible(False)
    # ax2.spines['right'].set_visible(False)
    # ax2.set_ylabel('Reward (a.u.)')
    # ax2.set_xlabel('Samples')
    # fig.tight_layout()

    # plt.draw()
    # plt.pause(.1)


#--------
# Saving 
#--------
save_path = '../data/delp_bao_rewards_best'
# plt.savefig('../figures/evaluate_dlep.png')
np.save(save_path, np.array(np_rewards_best))


plt.close()







