import sys
import math

import numpy as np
import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.utils as utils
from torch.distributions import Categorical
import matplotlib.pyplot as plt
import pandas as pd
from load_gp_objective import load_gp_objective

import time
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern, RBF, WhiteKernel, ConstantKernel as C

# np.random.random()
# acquisition_values = torch.from_numpy(np.array((1.,2.,3.,4.,5.,6.,7.,8.,9.,10., 11.,12.,13.,14.,15.,16.)).reshape(1,16)+10)

# print(acquisition_values)
# distr = Categorical(logits=acquisition_values)

# for i in range(100):
#     action_idx = distr.sample()
#     log_prob = distr.log_prob(action_idx)  
#     print(np.random.random())



# Set up the objective function
df = pd.read_csv("../data/cep_data.csv")
objective_data = df.to_numpy()
x_data = objective_data[:,(0,1)]
y_data = objective_data[:,2]


offset_1 = np.random.random() * 5 - 2.6 
offset_2 = np.random.random() * -150 + 30

offset = [offset_1, offset_2]
# offset = [0, 0]
print(offset)
gp_objective_model = load_gp_objective(x_data, y_data,offset)


