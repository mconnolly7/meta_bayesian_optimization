from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern, RBF, WhiteKernel, ConstantKernel as C
import numpy as np
import matplotlib.pyplot as plt

def load_gp_objective(x_data, y_data):
	x1 = np.linspace(1, 15, 100)
	x2 = np.linspace(0, 300, 100)
	input_space = np.array(np.meshgrid(x1, x2)).T.reshape(-1, 2)

	nd = 100;
	x1 = np.reshape(input_space[:,0], (nd,nd))
	x2 = np.reshape(input_space[:,1], (nd,nd))

	length_scale = np.std(x_data, axis=0) / np.sqrt(2)
	noise = np.std(y_data, axis=0) / np.sqrt(2)

	kernel =  Matern(length_scale=length_scale, nu=2.5, length_scale_bounds='fixed') + WhiteKernel(noise_level=noise,noise_level_bounds='fixed')
	# kernel =  Matern(length_scale=length_scale, nu=2.5) + WhiteKernel(noise_level=noise)
	
	gp_objective = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=1)

	# Fit to data to GP
	gp_objective = gp_objective.fit(x_data, y_data)

	return gp_objective