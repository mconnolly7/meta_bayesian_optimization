import numpy as np

def quadratic_objective(input, offset):

	output = (-1*( np.power(input[0] - offset[0],2) + np.power(input[1] - offset[1],2)) + 112.5/2)/1
	# output = (( np.power(input[0] - offset[0],2) + np.power(input[1] - offset[1],2)))/112.5

	return output

def easom_objective(input, offset):
	x1 = input[0]
	x2 = input[1]
	
	output = 10*np.cos(.1*(x1 - offset[0])) * np.cos(.1 * (x2 - offset[1])) * np.exp(-np.power(0.4*(x1 - offset[0]),2) - np.power(0.4*(x2 - offset[1]),2))
	return output

def gp_objective_function(input_, offset, gp_objective):

	offset_input = input_ - offset
	
	offset_input = offset_input.reshape(1,-1)
	
	output = gp_objective.predict(offset_input)

	return output[0]