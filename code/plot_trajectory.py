import numpy as np
import matplotlib
import matplotlib.pyplot as plt

load_path = '../data/total_reward_max.npy'
total_reward_max = np.load(load_path)

load_path = '../data/total_reward_min.npy'
total_reward_min = np.load(load_path)

load_path = '../data/total_reward_mean.npy'
total_reward_mean = np.load(load_path)

cmap = matplotlib.cm.get_cmap('viridis')

plt.rcParams.update({'font.size': 20})

fig, ax1 = plt.subplots(1,1, figsize=(7, 5))

ax1.plot(total_reward_max, color=cmap(.99))
ax1.plot(total_reward_min, color=cmap(0))
ax1.plot(total_reward_mean, color=cmap(0.5))

ax1.set_xlim([0,200])

ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(False)

ax1.set_xlabel('Training Batches (10 trials)')
ax1.set_ylabel('Reward')
fig.tight_layout()
plt.draw()

plt.savefig('../figures/training.png')
plt.close()
