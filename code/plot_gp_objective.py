import argparse, math, os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from load_gp_objective import load_gp_objective

# df = pd.read_csv("../data/cep_data.csv")
df = pd.read_csv("../data/dlep_data_2.csv")
objective_data = df.to_numpy()
x_data = objective_data[:,(0,1)]

x_data
y_data = objective_data[:,2]
y_data = y_data / np.max(y_data)
# Set up input space
x1 = np.linspace(40, 50, 100)
x2 = np.linspace(0, 300, 100)

input_space = np.array(np.meshgrid(x1, x2)).T.reshape(-1, 2)

# offset = [-2.6, 30]
offset = [0, 0]

gp_objective = load_gp_objective(x_data, y_data, offset)


nd = 100;
x1s = np.reshape(input_space[:,0], (nd,nd))
x2s = np.reshape(input_space[:,1], (nd,nd))

gp_mu = gp_objective.predict(input_space - offset)
gp_mu_max_idx = np.argmax(gp_mu)

a1 = np.reshape(gp_mu, (nd,nd))
cm1 = plt.cm.get_cmap('viridis')

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1, projection='3d')
surf1 = ax1.plot_surface(x1s, x2s, a1, cmap=cm1, linewidth=0, antialiased=False, alpha=.7)
ax1.scatter(x_data[:,0], x_data[:,1], y_data, color='r')
# ax1.set_xlim([1, 15])
# ax1.set_ylim([0, 300])

# ax1.set_xlim([40, 50])
# ax1.set_ylim([0, 300])

# ax1.set_zlim([0, 10])
ax1.set_xlabel('Depth (mm)')
ax1.set_ylabel('Amplitude (uA)')
ax1.set_zlabel('EP (normalized)')
plt.savefig('../figures/objective_{:.2f}_{:.2f}.png'.format(offset[0], offset[1]))
plt.show()

plt.close()

