import argparse, math, os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import torch
import torch.nn.utils as utils
import torch.nn as nn
from torch.autograd import Variable

from neural_acquisition_function import REINFORCE
from objective import quadratic_objective, easom_objective, gp_objective_function
from load_gp_objective import load_gp_objective

from tempfile import TemporaryFile

seed = 12
torch.manual_seed(seed)
np.random.seed(seed)

checkpoint_frequency = 10

dir = '../ckpt_' + 'test' 
if not os.path.exists(dir):    
    os.mkdir(dir)

# Training protocol
gamma = .99
learning_rate = .001

num_steps = 10
n_burn_in = 1
n_batch = 10
num_episodes = 10000

# Model architecture
n_objective_inputs = 2
n_gp_outputs = 2
n_sample_features = 2

n_af_inputs = n_objective_inputs + n_gp_outputs + n_sample_features
n_af_outputs = 1
hidden_size = 128

optimizer = REINFORCE(hidden_size, n_af_inputs, n_af_outputs, learning_rate)

objective_function = 'CEP'

if objective_function == 'CEP':
    # Set up the objective function
    df = pd.read_csv("../data/cep_data.csv")
    objective_data = df.to_numpy()
    x_data = objective_data[:,(0,1)]
    y_data = objective_data[:,2]

    # Set up input space
    x1 = np.linspace(1, 15, 100)
    x2 = np.linspace(0, 300, 100)

    gp_objective_model = load_gp_objective(x_data, y_data)

elif objective_function == 'easom':
    # Set up input space
    x1 = np.linspace(-5, 5, 100)
    x2 = np.linspace(-5, 5, 100)

input_space = np.array(np.meshgrid(x1, x2)).T.reshape(-1, n_objective_inputs)

total_reward_last = []
total_reward_best = []
total_reward_cumm = []

total_reward_max = []
total_reward_min = []
total_reward_mean = []

training_loss = []

plt.ion()    

for i_episode in range(num_episodes):
            
    batch_policy_loss = torch.zeros(1)
    batch_value_loss = torch.zeros(1)

    batch_log_prob = []
    batch_reward_last = []
    batch_reward_best = []
    batch_value = []
    
    # Run batch 
    for i_batch in range(n_batch):

        trial_log_prob = []
        trial_reward_last = []
        trial_reward_best = []
        trial_value = []
        
        x_data = []
        y_data = []
        x_max_data = []

        # Randomize offset within one quadrant of the parameter space
        offset_1 = np.random.random() * 5 - 2.6 
        offset_2 = np.random.random() * -150 + 30

        offset = [offset_1, offset_2]
        y_gt = gp_objective_model.predict(input_space - offset)

        y_gt_max = y_gt.max()
        y_gt_min = y_gt.min()


        # Run trial 
        for t in range(num_steps):

            next_input, log_prob, value_estimate = optimizer.select_action(x_data, y_data, input_space, n_burn_in, num_steps, i_episode, i_batch)

            # Get y data with noise based on next input
            if objective_function == 'CEP':
                y = gp_objective_function(next_input, offset, gp_objective_model) + .01*np.random.randn() 
                y = (y - y_gt_min) / (y_gt_max - y_gt_min)*10
            elif objective_function == 'easom':
                y = easom_objective(next_input, offset) + .01*np.random.randn() 

            y_data.append(y)
            x_data.append(next_input)

            # Calculate the best input so far
            y_max = max(y_data)
            y_max_idx = y_data.index(y_max)
            best_input = x_data[y_max_idx]
            x_max_data.append(best_input)

            reward_last = y

            if objective_function == 'CEP':
                reward_best = gp_objective_function(best_input, offset, gp_objective_model) + .01*np.random.randn()
                reward_best = (reward_best - y_gt_min) / (y_gt_max - y_gt_min)*10
            elif objective_function == 'easom':
                reward_best = easom_objective(best_input, offset) + .01*np.random.randn() 

            # Store probability, rewards, and value for this sample
            trial_log_prob.append(log_prob)
            
            trial_reward_last.append(reward_last)
            trial_reward_best.append(reward_best)
            trial_value.append(value_estimate)   

        # Compute the discounted rewards for the trial
        #   Can use either the last action rewards, or best action rewards for different behavior
        discounted_rewards = optimizer.discounted_rewards(trial_reward_last, gamma)
    
        # Organize for pytorch
        trial_value = torch.stack(trial_value).squeeze()
        trial_log_prob = torch.stack(trial_log_prob) 

        # Calculate the value loss
        loss_function = nn.MSELoss()
        value_loss = loss_function(trial_value, discounted_rewards)
        
        # Calculate the policy loss
        deltas = discounted_rewards #- trial_value.detach()

        policy_loss = -1 * deltas * trial_log_prob
        policy_loss = policy_loss.sum()
        
        # Sum losses and update parameters 
        batch_policy_loss = batch_policy_loss + policy_loss
        batch_value_loss = batch_value_loss + value_loss

        # Store data for the batch
        batch_reward_last.append(trial_reward_last)
        batch_reward_best.append(trial_reward_best)
        batch_value.append(value_estimate)   

    batch_loss = batch_policy_loss #+ batch_value_loss
    optimizer.update_parameters(batch_loss)

    cummulative_reward = np.mean(batch_reward_best, axis=0)

    total_reward_max.append(np.max(cummulative_reward))
    total_reward_min.append(np.min(cummulative_reward))
    total_reward_mean.append(np.mean(cummulative_reward))

    training_loss.append((batch_value_loss.data, batch_policy_loss.detach().data))

    # print(trial_value)
    # print(discounted_rewards)
    # print(deltas)
    # print(trial_log_prob)
    # print(policy_loss)
    
    print("Episode: {}, reward: {}".format(i_episode, total_reward_mean[i_episode]))

    if i_episode % checkpoint_frequency == 0:
        #----------
        # Plotting 
        #----------
        np_x_max_data = np.array(x_max_data)
        np_y_data = np.array(y_data)
        np_x_data = np.array(x_data)

        np_training_loss = np.array(training_loss)
        np_value  = trial_value.detach().numpy()
        np_rewards = discounted_rewards.detach().numpy()

        fig, (ax0, ax1, ax2, ax3, ax4) = plt.subplots(nrows=5, ncols=1, sharex=False, figsize=(12, 6))



        ax0.plot(np_x_data[:,0])
        ax0.plot(np_x_max_data[:,0])
        ax0.plot([0, num_steps-1], [offset[0], offset[0]], linestyle='dashed', linewidth=2)
        ax0.set_ylim([.9,15.1])
        # ax0.set_ylim([-5.1,5.1])

        ax1.plot(np_x_data[:,1])
        ax1.plot(np_x_max_data[:,1])
        ax1.plot([0, num_steps-1], [offset[1], offset[1]], linestyle='dashed', linewidth=2)
        ax1.set_ylim([-10, 310])
        # ax1.set_ylim([-5.1,5.1])

        ax2.plot(np_training_loss[:,1])
        ax2.plot(np_training_loss[:,1] + np_training_loss[:,0])
        ax3.plot(np_training_loss[:,0])
        ax3.set_yscale('log')

        ax4.plot(total_reward_max)
        ax4.plot(total_reward_min)
        ax4.plot(total_reward_mean)

        # plt.draw()
        # plt.pause(.1)

        #---------
        # Saving 
        #---------
        torch.save(optimizer.model.state_dict(), os.path.join(dir, 'reinforce-'+str(i_episode)+'.pkl'))
        
        plt.savefig('../figures/training.png')

        outfile = TemporaryFile()

        np.save('../data/total_reward_max.npy', np.array(total_reward_max))
        np.save('../data/total_reward_min.npy', np.array(total_reward_min))
        np.save('../data/total_reward_mean.npy', np.array(total_reward_mean))

    plt.close()









	
