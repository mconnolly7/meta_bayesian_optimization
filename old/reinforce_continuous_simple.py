import sys
import math

import numpy as np
import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.utils as utils
from torch.distributions import Categorical
import torchvision.transforms as T
import matplotlib.pyplot as plt

import time
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern, RBF, WhiteKernel, ConstantKernel as C

from torch.autograd import Variable

class Policy(nn.Module):
    def __init__(self, hidden_size, num_inputs, num_outputs):
        super(Policy, self).__init__()
       
        self.linear1 = nn.Linear(num_inputs, hidden_size).float()
        # self.linear2 = nn.Linear(hidden_size, hidden_size).float()
        # self.linear3 = nn.Linear(hidden_size, hidden_size).float()
        # self.linear4 = nn.Linear(hidden_size, hidden_size).float()
        self.linear5 = nn.Linear(hidden_size, num_outputs)

    def forward(self, inputs):
        x = inputs.float()
        # x = F.relu(self.linear1(x))
        # x = F.relu(self.linear2(x))

        # x = torch.relu(self.linear1(x))
        # x = torch.relu(self.linear2(x))
        # x = torch.relu(self.linear3(x))
        # x = torch.relu(self.linear4(x))

        x = torch.tanh(self.linear1(x))            
        # x = torch.tanh(self.linear2(x))
        # x = torch.tanh(self.linear3(x))
        # x = torch.tanh(self.linear4(x))

        mu = self.linear5(x)

        return mu


class REINFORCE:
    def __init__(self, hidden_size, num_inputs, num_outputs, learning_rate):
        self.model = Policy(hidden_size, num_inputs, num_outputs)
        self.model = self.model
        self.optimizer = optim.Adam(self.model.parameters(), lr=learning_rate)
        self.model.train()

    def select_action(self, x_data, y_data, input_space, n_burn_in, max_samples):

        if not x_data: 
            n_samples = 0
        else:
            n_samples = np.shape(x_data)[0]

        n_samples_feature = np.ones((input_space.shape[0], 1))*n_samples
        max_samples_feature = np.ones((input_space.shape[0], 1))*max_samples

        # Only fit the GP if enough samples and the length scales are non-zero
        if n_samples > n_burn_in:

            length_scale = np.std(x_data, axis=0) / np.sqrt(2)
    
            if ~np.any(length_scale == 0):
            
                kernel =  Matern(length_scale=length_scale, nu=1.5, length_scale_bounds='fixed') + WhiteKernel(noise_level=.01,noise_level_bounds='fixed')
                gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=10, normalize_y=True)

                # Fit to data using Maximum Likelihood Estimation of the parameters
                gp.fit(x_data, y_data)

                # Evaluate input on GP
                gp_mu, gp_sigma_sq = gp.predict(input_space, return_std=True)                 

                gp_mu = np.expand_dims(gp_mu, axis=1)
                gp_sigma_sq = np.expand_dims(gp_sigma_sq, axis=1)

        else:
            gp_mu = .01*np.random.randn(*n_samples_feature.shape)
            gp_sigma_sq = .01*np.random.randn(*n_samples_feature.shape)

        state = np.concatenate((input_space, gp_mu, gp_sigma_sq, n_samples_feature, max_samples_feature), axis=1)
        state = torch.from_numpy(state)

        acquisition_values = self.model(Variable(state))
        distr = Categorical(logits=acquisition_values.squeeze())
        action_idx = distr.sample()
        log_prob = distr.log_prob(action_idx)      
           
        # Select the next input based on the acquisition function
        next_input = input_space[action_idx.data,:]
     
        ############
        # Plotting #
        ############
        if n_samples > n_burn_in*-1:
            np_y_data = np.array(y_data)
            np_x_data = np.array(x_data)

            nd = 100;
            x1 = np.reshape(input_space[:,0], (nd,nd))
            x2 = np.reshape(input_space[:,1], (nd,nd))
            cm = plt.cm.get_cmap('RdYlBu')

            fig = plt.figure(figsize=plt.figaspect(0.5))
            a0 = np.reshape(acquisition_values.data.numpy(), (nd,nd))
            # a0 = np.reshape(gp_mu, (nd,nd))
            ax0 = fig.add_subplot(1, 2, 2, projection='3d')

            surf = ax0.plot_surface(x1, x2, a0, cmap=cm, linewidth=0, antialiased=False)
            # ax0.plot(np_x_data[:,0], np_x_data[:,1], np.zeros(np_x_data.shape[0]), linewidth=1, antialiased=False)
            fig.colorbar(surf)

            # ax0.scatter(next_input[0], next_input[1], 1.1)
            # ax0.view_init(90, 0)
            # ax0.set_zlim([0, 10])

            if n_samples == 0:
                plt.savefig('action0.png')
            if n_samples == 5:
                plt.savefig('action1.png')
            if n_samples == 9:
                a1 = np.reshape(gp_mu, (nd,nd))
                cm1 = plt.cm.get_cmap('RdYlBu')

                ax1 = fig.add_subplot(1, 2, 1, projection='3d')
                surf1 = ax1.plot_surface(x1, x2, a1, cmap=cm1, linewidth=0, antialiased=False)
                ax1.scatter(np_x_data[:,0], np_x_data[:,1], np_y_data)

                ax1.set_zlim([0, 10])

                plt.savefig('action2.png')



            plt.close()
        
        return next_input, action_idx, log_prob

    def update_parameters(self, loss):      
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

    def discounted_rewards(self, rewards, log_probs, gamma):
        R = torch.zeros(1)
        loss = 0

        # print(rewards)
        # rewards = (rewards - np.mean(rewards)) / np.std(rewards)

        for i in reversed(range(len(rewards))):
            R = gamma * R + rewards[i]
            loss = loss - log_probs[i].expand_as(R) * R

        loss = loss / len(rewards)
        return loss








