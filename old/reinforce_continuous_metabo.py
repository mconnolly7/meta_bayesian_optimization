import sys
import math

import numpy as np
import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.utils as utils
import torchvision.transforms as T
import matplotlib.pyplot as plt

import time
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern, RBF, WhiteKernel, ConstantKernel as C

from torch.autograd import Variable

# pi = Variable(torch.FloatTensor([math.pi])).cuda()
pi = Variable(torch.FloatTensor([math.pi]))

def normal(x, mu, sigma_sq):
    a0 = (-1*(Variable(mu)-mu).pow(2)/(2*sigma_sq)).exp()
    b0 = 1/(2*sigma_sq*pi.expand_as(sigma_sq)).sqrt()

    a1 = (-1*(Variable(x)-mu).pow(2)/(2*sigma_sq)).exp()
    b1 = 1/(2*sigma_sq*pi.expand_as(sigma_sq)).sqrt()

    # return (a1*b1)/(a0*b0)
    return (a1*b1)


class Policy(nn.Module):
    def __init__(self, hidden_size, num_inputs, num_outputs):
        super(Policy, self).__init__()
       
        self.linear1 = nn.Linear(num_inputs, hidden_size).float()
        # self.linear2 = nn.Linear(hidden_size, hidden_size)
        self.linear3 = nn.Linear(hidden_size, num_outputs)
        self.linear3_ = nn.Linear(hidden_size, num_outputs)

    def forward(self, inputs):
        x = inputs.float()
        x = F.relu(self.linear1(x))
        # x = F.relu(self.linear2(x))

        mu = self.linear3(x)
        sigma_sq = self.linear3_(x)

        mu = torch.tanh(mu)
        sigma_sq = torch.tanh(sigma_sq)
        return mu, sigma_sq


class REINFORCE:
    def __init__(self, hidden_size, num_inputs, num_outputs, learning_rate):
        self.model = Policy(hidden_size, num_inputs, num_outputs)
        self.model = self.model
        self.optimizer = optim.Adam(self.model.parameters(), lr=learning_rate)
        self.model.train()

    def select_action(self, x_data, y_data, input_space, n_burn_in):

        if not x_data: 
            n_samples = 0
        else:
            n_samples = np.shape(x_data)[0]
                    
        # print(x_data)

        gp = []

        # Only fit the GP if enough samples
        if n_samples > 3:
            
            length_scale = np.std(x_data, axis=0) / np.sqrt(2)
            
            if ~np.any(length_scale == 0):
                kernel =  Matern(length_scale=length_scale, nu=1.5, length_scale_bounds='fixed') + WhiteKernel(noise_level=.01,noise_level_bounds='fixed')
                gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=10, normalize_y=True)

                # Fit to data using Maximum Likelihood Estimation of the parameters
                gp.fit(x_data, y_data)

        gp_mu_ = []
        gp_mu_max = []
        gp_mu_max_idx = 0
        
        af_mu = []
        af_mu_ = []
        af_mu_max = []
        af_mu_max_idx = 0

        af_sigma_sq_ = []
        af_sigma_sq_max = []
        action_ = []
        action_max = []
        action_max_idx = 0

        # Iterate over the input space
        for i in range(input_space.shape[0]):

            x = [input_space[i,:]]
            
            # If we have a GP, use it to get mu and sigma
            if gp:

                # Evaluate input on GP
                gp_mu, gp_sigma_sq = gp.predict(x, return_std=True)                 
                gp_mu_.append(gp_mu)

                # Select max gp for best input 
                if i == 0 or gp_mu > gp_mu_max :
                    gp_mu_max = gp_mu
                    gp_mu_max_idx = i

            # Otherwise generate random output for mu and sigma
            else:
                gp_mu = [.01*np.random.randn()]
                gp_sigma_sq = [.01*np.random.randn()]

            # Define state as the gp x input, mu, sigma, and t
            # state = np.concatenate((x[0], gp_mu, gp_sigma_sq, [n_samples]))
            state = np.concatenate(x[0])
            state = torch.tensor(state)
            
            # Use policy to provide an aquisition score and probability
            af_mu, af_sigma_sq = self.model(Variable(state))

            # Generate a random perturbation of the policy based on sigma
            af_sigma_sq = F.softplus(af_sigma_sq)
            eps = torch.randn(af_mu.size())
            action = (af_mu + af_sigma_sq.sqrt()*Variable(eps)).data
            # action = (af_mu + .0*Variable(eps)).data

            af_mu_.append(af_mu.data)
            action_.append(action)
            af_sigma_sq_.append(af_sigma_sq.data)

            # Select max action score
            if i == 0 or action > action_max :
                action_max = action
                action_max_idx = i

                af_mu_max = af_mu
                af_sigma_sq_max = af_sigma_sq
        
        # prob = normal(action_max, af_mu_max, af_sigma_sq_max)
        prob = af_mu_max + af_sigma_sq_max
        entropy = -0.5*((af_sigma_sq_max+2*pi.expand_as(af_sigma_sq_max)).log()+1)
        # log_prob = prob.log()
        log_prob = prob

        # print(action_max)
        # print(af_mu_max)
        # print(af_sigma_sq_max)
        # print(prob)
        # print(log_prob)
        
        # Select the next input based on the acquisition function
        next_input = input_space[action_max_idx,:]
        # next_input = [3.5, 3.5]

        # Select the estimated optimal
        
        # Based on the GP if > burn in
        best_input = input_space[gp_mu_max_idx,:]

        np_y_data = np.array(y_data)
        np_x_data = np.array(x_data)
        
        x1 = np.reshape(input_space[:,0], (20,20))
        x2 = np.reshape(input_space[:,1], (20,20))
        cm = plt.cm.get_cmap('RdYlBu')

        fig = plt.figure(figsize=plt.figaspect(0.5))

        a0 = np.reshape(af_mu_, (20,20))
        ax0 = fig.add_subplot(1, 2, 1, projection='3d')
        ax0.plot_surface(x1, x2, a0, cmap=cm)
        ax0.scatter(input_space[action_max_idx,0], input_space[action_max_idx,1], af_mu_max.data+.1)
        a1 = np.reshape(af_sigma_sq_, (20,20))
        ax1 = fig.add_subplot(1, 2, 2, projection='3d')
        ax1.plot_surface(x1, x2, a1, cmap=cm)
            
        if n_samples == 0:
            plt.savefig('action0.png')
        if n_samples == 1:
            plt.savefig('action1.png')
        if n_samples == 2:
            plt.savefig('action2.png')

        plt.close()
        
        return next_input, best_input, action, log_prob, entropy

    def update_parameters(self, rewards, log_probs, entropies, gamma):
        R = torch.zeros(1)
        loss = 0
        # print(log_probs)
        
        
        for i in reversed(range(len(rewards))):
            # R = gamma * R + rewards[i]
            R = R + rewards[i]
            loss = loss - log_probs[i] * (Variable(R).expand_as(log_probs[i]))
            # print(log_probs)
        
        # for i in reversed(range(len(rewards))):
            # R = rewards[i] + R
            
            # print(log_probs[i]*(Variable(R).expand_as(log_probs[i])))
            # # loss = loss - (log_probs[i]*(Variable(R).expand_as(log_probs[i]))).sum() - (0.0001*entropies[i]).sum()
            # loss = loss - (log_probs[i]*(Variable(R).expand_as(log_probs[i])))
            #             # R = gamma * R + rewards[i]

            # loss = loss - ((Variable(R).expand_as(log_probs[i]))).sum() - (0.0001*entropies[i]).sum()
            # loss = loss + (log_probs[i]*(Variable(R).expand_as(log_probs[i]))).sum() - (0.0001*entropies[i]).sum()
            # loss = loss - Variable(R)

        print(loss)
        loss = loss / len(rewards)
        
        self.optimizer.zero_grad()
        loss.backward()
        utils.clip_grad_norm_(self.model.parameters(), 40)
        self.optimizer.step()
        return loss







