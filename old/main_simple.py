import argparse, math, os
import numpy as np
import matplotlib.pyplot as plt

import torch
from torch.autograd import Variable
import torch.nn.utils as utils

from reinforce_continuous_simple import REINFORCE
from objective import quadratic_objective, easom_objective
from tempfile import TemporaryFile

gamma = .99
num_steps = 10
seed = 12
num_episodes = 100000
hidden_size = 128
learning_rate = .001
checkpoint_frequency = 10

n_objective_inputs = 2

n_gp_outputs = 2
n_sample_features = 2

n_af_inputs = n_objective_inputs + n_gp_outputs + n_sample_features
n_af_outputs = 1
n_burn_in = 3

torch.manual_seed(seed)
np.random.seed(seed)

optimizer = REINFORCE(hidden_size, n_af_inputs, n_af_outputs, learning_rate)

x1 = np.linspace(-5, 5, 100)
input_space = np.array(np.meshgrid(x1, x1)).T.reshape(-1, n_objective_inputs)

dir = 'ckpt_' + 'test' 
if not os.path.exists(dir):    
    os.mkdir(dir)

rewards_ = []
loss_ = []
x_data_ = []
y_data_ = []
total_reward = []
training_loss = []

for i_episode in range(num_episodes):
    
    
    batch_loss = []

    # Run multiple trials in a batch
    for i_batch in range(2) :
        
        if i_batch >= 0 :
            offset = [-2, 2]
        else:
            offset = [2, -2]

        trial_log_prob = []
        trial_reward = []
        
        x_data = []
        y_data = []
        x_max_data = []

        # Run trial 
        for t in range(num_steps):

            next_input, action, log_prob = optimizer.select_action(x_data, y_data, input_space, n_burn_in, num_steps)

            # Get y data with noise based on next input
            y = easom_objective(next_input, offset) 
            
            y_data.append(y)
            x_data.append(next_input)
            x_data_.append(next_input)

            # Calculate reward based on the noiseless best input
            y_max = max(y_data)
            y_max_idx = y_data.index(y_max)
            best_input = x_data[y_max_idx]
            x_max_data.append(best_input)

            reward = easom_objective(best_input, offset) + .01*np.random.randn()

            # Store probability and rewards for this sample
            trial_log_prob.append(log_prob)
            trial_reward.append(reward)
        
        # Compute the discounted loss for the trial
        loss = optimizer.discounted_rewards(trial_reward, trial_log_prob, gamma)
        batch_loss.append(loss)
        total_reward.append(np.mean(trial_reward))

    # Update policy network based on batch loss
    total_loss = batch_loss[0] + batch_loss[1]
    training_loss.append(total_loss.data)

    # print(total_loss)
    optimizer.update_parameters(total_loss)

    ############
    # Plotting #
    ############
    print("Episode: {}, reward: {}".format(i_episode, total_reward[i_episode]))

    np_x_max_data = np.array(x_max_data)
    np_y_data = np.array(y_data)
    np_x_data = np.array(x_data)

    fig, (ax0, ax1, ax3, ax4) = plt.subplots(nrows=4, ncols=1, sharex=False, figsize=(12, 6))

    ax0.plot(np_x_data[:,0])
    ax0.plot(np_x_max_data[:,0])
    ax0.plot([0, num_steps-1], [offset[0], offset[0]], linestyle='dashed', linewidth=2)
    ax0.set_ylim([-5.2, 5.2])

    ax1.plot(np_x_data[:,1])
    ax1.plot(np_x_max_data[:,1])
    ax1.plot([0, num_steps-1], [offset[1], offset[1]], linestyle='dashed', linewidth=2)
    ax1.set_ylim([-5.2, 5.2])

    ax3.plot(training_loss)
    # ax3.set_ylim([0, 10.5])

    N = 10
    smooth_reward = np.convolve(total_reward, np.ones(N)/N, mode='valid')

    ax4.plot(total_reward)
    ax4.plot(smooth_reward)
    ax4.set_ylim([0, 10.5])

    plt.savefig('foo_flip.png')
    plt.close()

    ##########
    # Saving #
    ##########
    if i_episode % checkpoint_frequency == 0:
        torch.save(optimizer.model.state_dict(), os.path.join(dir, 'reinforce-'+str(i_episode)+'.pkl'))

        outfile = TemporaryFile()

        np.save('total_reward.npy', np.array(total_reward))
        np.save('smooth_reward.npy', np.array(smooth_reward))








	
