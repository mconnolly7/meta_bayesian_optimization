import argparse, math, os
import numpy as np
import matplotlib.pyplot as plt

import torch
from torch.autograd import Variable
import torch.nn.utils as utils

from normalized_actions import NormalizedActions
from reinforce_continuous_metabo import REINFORCE
from objective import quadratic_objective

parser = argparse.ArgumentParser(description='PyTorch REINFORCE example')
parser.add_argument('--gamma', type=float, default=0.01 , metavar='G',
                    help='discount factor for reward (default: 0.99)')
parser.add_argument('--exploration_end', type=int, default=100, metavar='N',
                    help='number of episodes with noise (default: 100)')
parser.add_argument('--seed', type=int, default=1234, metavar='N',
                    help='random seed (default: 123)')
parser.add_argument('--num_steps', type=int, default=10, metavar='N',
                    help='max episode length (default: 50)')
parser.add_argument('--num_episodes', type=int, default=10000, metavar='N',
                    help='number of episodes (default: 1)')
parser.add_argument('--hidden_size', type=int, default=128, metavar='N',
                    help='number of episodes (default: 64)')
parser.add_argument('--render', action='store_true',
                    help='render the environment')
parser.add_argument('--ckpt_freq', type=int, default=10, 
		    help='model saving frequency')
parser.add_argument('--display', type=bool, default=False,
                    help='display or not')

gamma = .99
num_steps = 3
seed = 12345
num_episodes = 100000
hidden_size = 128
learning_rate = .0001

torch.manual_seed(seed)
np.random.seed(seed)

x1 = np.linspace(-5, 5, 20)

n_objective_inputs = 2

n_af_inputs = n_objective_inputs + 2 + 1
n_af_outputs = 1
n_burn_in = 3

optimizer = REINFORCE(hidden_size, n_af_inputs, n_af_outputs, learning_rate)

input_space = np.array(np.meshgrid(x1, x1)).T.reshape(-1, n_objective_inputs)

dir = 'ckpt_' + 'test' 
if not os.path.exists(dir):    
    os.mkdir(dir)

rewards_list = []
loss_ = []
for i_episode in range(num_episodes):
    
    entropies   = []
    log_probs   = []
    rewards     = []
    x_max_data  = []
    x_data      = []
    y_data      = []

    # offset_1    = np.random.random([1,1000]) * 10 - 5
    # offset_2    = np.random.random([1,1000]) * 10 - 5

    # offset_norm = abs(offset_1 * offset_1 + offset_2 * offset_2 - 4.5*4.5)

    # min_        = min(offset_norm[0])

    # min_idx     = np.where(offset_norm[0] == min_)
    # min_idx     = [min_idx[0][0]]

    # offset_1    = offset_1[0][min_idx]
    # offset_2    = offset_2[0][min_idx]

    offset_1    = np.random.random([1])/10 - 3.5
    offset_2    = np.random.random([1])/10 - 3.5

    offset      = [offset_1, offset_2]

    for t in range(num_steps):

        next_input, best_input, action, log_prob, entropy = optimizer.select_action(x_data, y_data, input_space, n_burn_in)
        
        # log_prob = log_prob
        # Get y data with noise based on next input
        y = quadratic_objective(next_input, offset) + .01*np.random.randn()
        
        y_data.append(y)
        x_data.append(next_input)

        # if t <= n_burn_in:
        y_max = max(y_data)
        y_max_idx = y_data.index(y_max)
        best_input = x_data[y_max_idx]
       
        # best_input = [-3.5, -3.5]

        x_max_data.append(best_input)
    
        # Calculate reward based on the noiseless best input
        reward = quadratic_objective(best_input, offset)
        
        entropies.append(entropy)
        log_probs.append(log_prob)
        rewards.append(reward)
    
    # Update the parameters after each episode/trial
    loss = optimizer.update_parameters(rewards, log_probs, entropies, gamma)
    
    # rewards_list.append(np.sum(rewards))
    rewards_list.append(loss)
    print("Episode: {}, reward: {}".format(i_episode, rewards_list[i_episode]))

    # print(loss)
    # if i_episode % args.ckpt_freq == 0:
    #     torch.save(optimizer.model.state_dict(), os.path.join(dir, 'reinforce-'+str(i_episode)+'.pkl'))

    np_x_max_data = np.array(x_max_data)
    np_y_data = np.array(y_data)
    np_x_data = np.array(x_data)
    np_rewards = np.array(rewards)

    fig, (ax0, ax1, ax3, ax4) = plt.subplots(nrows=4, ncols=1, sharex=False, figsize=(12, 6))

    ax0.plot(np_x_data[:,0])
    ax0.plot(np_x_max_data[:,0])
    ax0.plot([0, num_steps-1], [offset[0], offset[0]], linestyle='dashed', linewidth=2)
    ax0.set_ylim([-5.2, 5.2])

    ax1.plot(np_x_data[:,1])
    ax1.plot(np_x_max_data[:,1])
    ax1.plot([0, num_steps-1], [offset[1], offset[1]], linestyle='dashed', linewidth=2)
    ax1.set_ylim([-5.2, 5.2])

    # ax2.plot(np_x_data[:,2])   
    # ax2.plot(np_x_max_data[:,2])
    # ax2.plot([1, 50], [offset[2], offset[2]], linestyle='dashed', linewidth=2)
    # ax2.set_ylim([-1, 1])

    ax3.plot(np_y_data)
    ax3.plot(np_rewards)
    # ax3.set_ylim([-5.1, 5.1])

    # ax3.plot([0, 9], [0,0], linestyle='dashed', linewidth=2)

    ax4.plot(rewards_list)
    plt.savefig('foo_flip.png')
    plt.close()
    # fig2 = plt.figure()
    # ax = fig2.add_subplot(projection='3d')
    # ax.scatter(np_x_data[:,0], np_x_data[:,1], np_y_data)
    # plt.show()

	
